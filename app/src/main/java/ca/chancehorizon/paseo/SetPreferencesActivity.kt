package ca.chancehorizon.paseo

import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity



class SetPreferencesActivity : AppCompatActivity() {

    // get the application settings (save messages etc)
    lateinit var paseoPrefs : SharedPreferences
    // watch for changes to saved preferences (for changing theme)
    lateinit var listener: SharedPreferences.OnSharedPreferenceChangeListener



    override fun onCreate(savedInstanceState: Bundle?) {

        // change the colour scheme used based on the theme selected by the user in the settings
        //  (saved in shared preferences)
        changeTheme()

        super.onCreate(savedInstanceState)

        supportFragmentManager.beginTransaction().replace(android.R.id.content,
                PrefsFragment()).commit()

    }



    override fun onPostCreate(savedInstanceState: Bundle?) {

        super.onPostCreate(savedInstanceState)

        // get the application settings (save messages etc)
        paseoPrefs = this.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)

        // check for changes to the theme settings
        listener = SharedPreferences.OnSharedPreferenceChangeListener { _, key ->
            when (key) {
                "prefTheme", "prefDarkTheme" -> {
                    changeTheme()
                    recreate()
                }
            }
        }

        paseoPrefs.registerOnSharedPreferenceChangeListener(listener)
    }



    override fun onResume() {
        super.onResume()

        paseoPrefs.registerOnSharedPreferenceChangeListener(listener)
    }



    override fun onPause () {
        super.onPause()

        paseoPrefs.unregisterOnSharedPreferenceChangeListener(listener)
    }



    // change the colour scheme used throughout paseo
    fun changeTheme()
    {
        // get the application settings (save messages etc)
        paseoPrefs = this.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)

        var theme = paseoPrefs.getString("prefTheme", "1").toInt()
        var dark = paseoPrefs.getBoolean("prefDarkTheme", false)

        // it would be better if the theme values settings were stored in values xml file
        //  rather than hardcoded here
        //  that way they could be more easily added/modified in the future without having to change
        //  code in this function
        var themeSetting = if (theme <= 10) theme else (Math.random()*10).toInt()

        when (themeSetting) {
            1 -> {
                setTheme(R.style.Theme_Paseo_Blue)
            }
            2 -> {
                setTheme(R.style.Theme_Paseo_Green)
            }
            3 -> {
                setTheme(R.style.Theme_Paseo_Red)
            }
            4 -> {
                setTheme(R.style.Theme_Paseo_Orange)
            }
            5 -> {
                setTheme(R.style.Theme_Paseo_Purple)
            }
            6 -> {
                setTheme(R.style.Theme_Paseo_Pink)
            }
            7 -> {
                setTheme(R.style.Theme_Paseo_Yellow)
            }
            8 -> {
                setTheme(R.style.Theme_Paseo_Brown)
            }
            9 -> {
                setTheme(R.style.Theme_Paseo_Grey)
            }
            10 -> {
                setTheme(R.style.Theme_Paseo_BlueGrey)
            }
        }

        if (dark) {
            getTheme().applyStyle(R.style.OverlayDark, true);
            getTheme().applyStyle(R.style.PaseoDialog, true);
        }
    }
}

