package ca.chancehorizon.paseo.background

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.appwidget.AppWidgetManager
import android.content.*
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.speech.tts.TextToSpeech
import android.speech.tts.Voice
import android.util.Log
import android.widget.RemoteViews
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import ca.chancehorizon.paseo.*
/* *** need to be re-written without any need for google play services (need to write own activity detection)
import com.google.android.gms.location.ActivityRecognitionClient
import com.google.android.gms.location.DetectedActivity
*/
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.max



class StepCounterService : Service(), SensorEventListener, TextToSpeech.OnInitListener {

    private val SERVICE_ID = 1001

    var running = false
    var sensorManager: SensorManager? = null
    var startSteps = 0
    var currentSteps = 0
    var endSteps = 0
    val targetSteps = 10000 // *** this should be overridden by the user set value fromthe PASEO settings screen
    var latestDay = 0
    var latestHour = 0

    lateinit var paseoDBHelper : PaseoDBHelper

    private var tts: TextToSpeech? = null
    private var ttsAvailable = false


    internal var mBinder: IBinder = LocalBinder()



    inner class LocalBinder : Binder() {
        val serverInstance: StepCounterService
            get() = this@StepCounterService
    }



    @Nullable
    override fun onBind(intent: Intent): IBinder? {
        return mBinder
    }



    override fun onCreate() {
        super.onCreate()

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager

        running = true
        var stepsSensor = sensorManager?.getDefaultSensor(Sensor.TYPE_STEP_COUNTER)

        if (stepsSensor == null) {
            Toast.makeText(this, "No Step Counter Sensor !", Toast.LENGTH_SHORT).show()
        } else {
            sensorManager?.registerListener(this, stepsSensor, SensorManager.SENSOR_DELAY_UI)
        }

        // point to the Paseo database that stores all the daily steps data
        paseoDBHelper = PaseoDBHelper(this)

        try {
            tts = TextToSpeech(this, this)
        }
        catch (e: Exception) {
            tts = null
        }
    }



    override fun onInit(status: Int) {

        val paseoPrefs = this.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)

        // set up the text to speech voice
        if (status == TextToSpeech.SUCCESS) {
            val result = tts?.setLanguage(Locale.US)
            if (result == TextToSpeech.LANG_MISSING_DATA ||
                    result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "Language not supported")
            }

            ttsAvailable = true
        } else {
            Log.e("TTS", "Initialization failed")
            ttsAvailable = false
        }

        // update shared preferences to not show first run dialog again
        val edit: SharedPreferences.Editor = paseoPrefs!!.edit()
        edit.putBoolean("prefTTSAvailable", ttsAvailable)
        edit.apply()
    }



    override fun onDestroy() {

        // turn off step counter service
        stopForeground(true)

        // turn off auto start service
        val broadcastIntent = Intent()
        broadcastIntent.action = "restartservice"
        broadcastIntent.setClass(this, Restarter::class.java)
        this.sendBroadcast(broadcastIntent)

        // Shutdown TTS
        if (tts != null) {
            tts!!.stop()
            tts!!.shutdown()
        }

        super.onDestroy()
    }



    override fun onTaskRemoved(rootIntent: Intent?) {
        super.onTaskRemoved(rootIntent)
        stopSelf()
    }



    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        startForegroundServiceWithNotification()

        super.onStartCommand(intent, flags, startId)

        return Service.START_STICKY
    }



    private fun startForegroundServiceWithNotification() {
        val resultIntent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val builder: NotificationCompat.Builder
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelName = getString(R.string.notification_channel_step_count)
            val importance = NotificationManager.IMPORTANCE_LOW
            val channelId = "PASEO_CHANNEL_ID"
            val channel = NotificationChannel(channelId, channelName, importance)
            channel.setShowBadge(true)

            builder = NotificationCompat.Builder(this, channelId)

            val notificatioManager = getSystemService(NotificationManager::class.java)
            notificatioManager.createNotificationChannel(channel)
        } else {
            builder = NotificationCompat.Builder(this)
        }

        val icon = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) R.mipmap.ic_launcher_nb else R.mipmap.ic_notification

        var DateFormat = SimpleDateFormat("yyyyMMdd") // looks like "19891225"
        val today = DateFormat.format(Date()).toInt()

        // if currentSteps > startSteps then
        //  do not need to create a new record
        val theSteps = paseoDBHelper.getDaysSteps(today)

        with(builder) {
            builder.setContentTitle("" + NumberFormat.getIntegerInstance().format(theSteps) + " steps today. ")
            builder.setStyle(NotificationCompat.BigTextStyle().bigText("Target: " +
                    NumberFormat.getIntegerInstance().format(targetSteps) + ". " +
                    NumberFormat.getIntegerInstance().format(max(targetSteps - theSteps, 0)) +
                    " to go! "))
            setWhen(System.currentTimeMillis())
            setSmallIcon(icon)
            setContentIntent(pendingIntent)
            priority = NotificationCompat.PRIORITY_LOW
            setVisibility(NotificationCompat.VISIBILITY_SECRET)
        }

        val notification = builder.build()

        startForeground(SERVICE_ID, notification)
    }



    // needed for step counting (even though it is empty
    override fun onAccuracyChanged(p0: Sensor?, p1: Int)
    {
    }



    // override fun onSensorChanged(event: SensorEvent)
    override fun onSensorChanged(event: SensorEvent)
    {

        if (running)
        {
            var DateFormat = SimpleDateFormat("yyyyMMdd") // looks like "19891225"
            val today = DateFormat.format(Date()).toInt()
            DateFormat = SimpleDateFormat("HH")
            val currentHour = DateFormat.format(Date()).toInt()

            // read the step count value from the devices step counter sensor
            currentSteps = event.values[0].toInt()

            // get the latest step information from the database
            if (paseoDBHelper.readRowCount() > 0) {
                latestDay = paseoDBHelper.readLastStepsDate()
                latestHour = paseoDBHelper.readLastStepsTime()
                startSteps = paseoDBHelper.readLastStartSteps()
                endSteps = paseoDBHelper.readLastEndSteps()
            }

            // hour is one more than last hour recorded -> add new hour record to database
            if(today == latestDay && currentHour == latestHour + 1 && currentSteps >= startSteps) {
                addSteps(today, currentHour, endSteps, currentSteps)
            }
            // add a new hour record (may be for current day or for a new day)
            //  also add a new record if the current steps is less than the most recent start steps (happens when phone has been rebooted)
            else if (today != latestDay || currentHour != latestHour || currentSteps < startSteps) {
                addSteps(today, currentHour, currentSteps, currentSteps)
            }
            else {
                //  set endSteps to current steps (update the end steps for current hour)
                addSteps(today, currentHour, 0, currentSteps, true)
            }

            // if currentSteps > startSteps then
            //  do not need to create a new record
            val theSteps = paseoDBHelper.getDaysSteps(today)

            // update the step count in the widget
            val context = this
            val appWidgetManager = AppWidgetManager.getInstance(context)
            val remoteViews = RemoteViews(context.packageName, R.layout.paseo_steps_widget)
            val thisWidget = ComponentName(context, PaseoStepsWidget::class.java)
            remoteViews.setTextViewText(R.id.stepwidget_text, "" + NumberFormat.getIntegerInstance().format(theSteps))
            appWidgetManager.updateAppWidget(thisWidget, remoteViews)

            // update the notification with new step count
            val builder: NotificationCompat.Builder
            val channelId = "PASEO_CHANNEL_ID"

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channelName = getString(R.string.notification_channel_step_count)
                val importance = NotificationManager.IMPORTANCE_LOW
                val channel = NotificationChannel(channelId, channelName, importance)
                val notificationManager = getSystemService(NotificationManager::class.java)
                val icon = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) R.mipmap.ic_launcher_nb else R.mipmap.ic_notification

                builder = NotificationCompat.Builder(this, channelId)
                builder.setContentTitle("" + NumberFormat.getIntegerInstance().format(theSteps) + " steps today. ")
                builder.setStyle(NotificationCompat.BigTextStyle().bigText("Target: " +
                        NumberFormat.getIntegerInstance().format(targetSteps) + " . " +
                        NumberFormat.getIntegerInstance().format(max(targetSteps - theSteps, 0)) +
                        " to go! "))
                builder.setSmallIcon(icon)

                try {
                    notificationManager.notify(SERVICE_ID, builder.build())
                }
                catch (e : Exception)
                {
                    print (e)
                }
            }

            // check if the user has a mini goal running and update all of the values needed
            checkMiniGoal()

            // send message to application activity so that it can react to new steps being sensed
            val local = Intent()

            local.action = "ca.chancehorizon.paseo.action"
            local.putExtra("data", theSteps)
            this.sendBroadcast(local)
        }

    }



    // update mini goal fragement and speak alerts when goal or step interval achieved
    fun checkMiniGoal() {

        val paseoPrefs = this.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)

        val isGoalActive = paseoPrefs!!.getBoolean("prefMiniGoalActive", false)

        if (isGoalActive) {

            // get the mini goal settings
            val miniGoalSteps = paseoPrefs!!.getInt("prefMiniGoalSteps", 20)

            // get the mini goal settings
            val miniGoalAlertInterval = paseoPrefs!!.getInt("prefMiniGoalAlertInterval", 0)

            // get the mini goal settings
            var miniGoalNextAlert = paseoPrefs!!.getInt("prefMiniGoalNextAlert", 0)

            val miniGoalStartSteps = paseoPrefs!!.getInt("prefMiniGoalStartSteps", 0)
            val miniGoalStartTime = paseoPrefs!!.getInt("prefMiniGoalStartTime", 2000)

            var stepCount = 0
            // display current step count
            val miniGoalEndSteps = paseoDBHelper.readLastEndSteps()
            if (miniGoalSteps != null) {
                // display goal step count
                stepCount = miniGoalEndSteps - miniGoalStartSteps
            }

            // check if mini goal has been achieved
            if (stepCount >= miniGoalSteps) {
                // update shared preferences to save the next alert steps
                var edit: SharedPreferences.Editor = paseoPrefs!!.edit()
                edit.putBoolean("prefMiniGoalActive", false)
                edit.apply()

                speakOut("Congratulations on " + miniGoalSteps.toString() + " steps!")
            }
            // check if mini goal has been achieved (else used so that it is not checked when goal achieved)
            else if ((stepCount >= miniGoalNextAlert) && miniGoalAlertInterval > 0) {
                // update shared preferences to save the next alert steps
                var edit: SharedPreferences.Editor = paseoPrefs!!.edit()

                speakOut(miniGoalNextAlert.toString() + " steps!")

                miniGoalNextAlert = miniGoalNextAlert + miniGoalAlertInterval
                edit.putInt("prefMiniGoalNextAlert", miniGoalNextAlert)
                edit.apply()

            }
        }
    }



    // insert or update a steps record in the Move database
    fun addSteps(date: Int = 0, time: Int = 0, startSteps: Int = 0, endSteps: Int = 0, update: Boolean = false)
    {

        // update the endsteps for the current hour
        if (update)
        {
            var result = paseoDBHelper.updateEndSteps(StepsModel(0, date = date, hour = time,
                    startSteps = startSteps, endSteps = endSteps))
        }
        else
        {
            var result = paseoDBHelper.insertSteps(StepsModel(0, date = date, hour = time,
                    startSteps = startSteps, endSteps = endSteps))
        }

        latestDay = date
    }


    // use text to speech to "speak" some text
    fun speakOut(theText : String) {
        if(theText != null) {

            val paseoPrefs = this.getSharedPreferences("ca.chancehorizon.paseo_preferences", 0)

            val ttsPitch = paseoPrefs!!.getFloat("prefVoicePitch", 100F)
            val ttsRate = paseoPrefs!!.getFloat("prefVoiceRate", 100F)

            // set the voice to use to speak with
            val ttsVoice = paseoPrefs!!.getString("prefVoiceLanguage", "en_US - en-US-language")
            val ttsLocale1 = ttsVoice.substring(0, 2)
            val ttsLocale2 = ttsVoice.substring(3)
            val voiceobj = Voice(ttsVoice, Locale(ttsLocale1, ttsLocale2), 1, 1, false, null)
            tts?.setVoice(voiceobj)

            tts?.setPitch(ttsPitch / 100)
            tts?.setSpeechRate(ttsRate / 100)

            var ttsResult = tts?.speak(theText, TextToSpeech.QUEUE_FLUSH, null, "")

            if(ttsResult == -1) {
                tts = TextToSpeech(this, this)
                ttsResult = tts?.speak(theText, TextToSpeech.QUEUE_FLUSH, null, "")
            }
        }
    }
}
